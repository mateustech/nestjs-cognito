import { Controller, Get } from '@nestjs/common';
import axios from 'axios';
import { configTasy } from 'src/configs/tasy';

@Controller('tasy')
export class TasyController {
  @Get('api')
  teste() {
    return 'Hello Tasy';
  }

  @Get()
  tasy() {
    return axios
      .post(`${configTasy?.host}:${configTasy.port}/newapp/oauth2/authorize`, {
        client_id: 'clientid',
        response_type: 'code',
        scope: 'create',
        provision_key: 'provisionkey',
        authenticated_userid: '1',
        redirect_uri: 'http://192.168.31.216:52773/newapp/agenda/cancelar',
      })
      .then(function(response) {
        return { ok: response.data?.redirect_uri?.length };
      })
      .catch(function(error) {
        console.error(error);
        return { error };
      });
  }
}

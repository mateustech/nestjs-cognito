import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AppService } from 'src/services/app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('private')
  @UseGuards(AuthGuard('patient'))
  getPrivate(): string {
    return this.appService.getHello();
  }

  @Get('public')
  getPublic(): string {
    return this.appService.getHello();
  }
}

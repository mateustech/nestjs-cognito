import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from 'src/controllers/app.controller';
import { AppService } from 'src/services/app.service';
import { AuthModule } from './auth.module';
import { TasyModule } from './tasy.module';
import { UsersModule } from './users.module';

@Module({
  imports: [
    AuthModule,
    TasyModule,
    UsersModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

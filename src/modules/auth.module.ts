import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthConfig } from 'src/configs/auth';
import { AuthController } from 'src/controllers/auth.controller';
import { DoctorStrategy } from 'src/middleware/doctor.strategy';
import { PatientStrategy } from 'src/middleware/patient.strategy';
import { AuthService } from 'src/services/auth.service';

@Module({
  imports: [PassportModule],
  providers: [AuthConfig, AuthService, PatientStrategy, DoctorStrategy],
  controllers: [AuthController],
})
export class AuthModule {}

import { Module } from '@nestjs/common';
import { TasyController } from 'src/controllers/tasy.controller';

@Module({
  controllers: [TasyController],
  providers: [],
})
export class TasyModule {}
